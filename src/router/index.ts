import { createRouter, createWebHistory } from 'vue-router'
import Layout from '@/layout/index.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      component: () => import('@/views/login.vue'),
      meta: { "title": "登录" },
      hidden: true
    },
    {
      name: 'Index',
      path: '',
      component: Layout,
      redirect: '/index',
      meta: { "title": "首页" },
      children: [
        {
          path: 'index',
          component: () => import('@/views/home/index.vue'),
          name: 'Index',
          meta: { title: '首页', icon: 'home', affix: true }
        }
      ]
    },
    {
      name: 'System',
      path: '/system',
      component: Layout,
      redirect: 'noredirect',
      meta: { "title": "系统管理" },
      children: [
        {
          name: 'User',
          path: 'user',
          component: () => import('@/views/system/user.vue'),
          meta: { title: '用户管理', icon: '' }
        },
        {
          name: 'Role',
          path: 'role',
          component: () => import('@/views/system/role.vue'),
          meta: { title: '角色管理', icon: '' }
        },
        {
          name: 'Menu',
          path: 'menu',
          component: () => import('@/views/system/menu.vue'),
          meta: { title: '菜单管理', icon: '' }
        },
      ]
    },
  ]
})

export default router
